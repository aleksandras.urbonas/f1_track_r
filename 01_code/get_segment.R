# t: get_segment.R
# v: 2022-09-09T1840 AU

{
  # DONE: get start and end
  sO_x=O_x;sO_y=O_y;sO_xy=c(sO_x,sO_y);
  sB_x=B_x;sB_y=B_y;sB_xy=c(sB_x,sB_y);
  cat("+ created line segment:\n")
  cat("* start #", this_point, ": ", sO_xy)
  cat("* end   #", next_point, ": ", sB_xy)
  cat(DELIM)
}
