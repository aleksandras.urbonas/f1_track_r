# PLOT COORDINATES 

## Setup up coordinate system (with x == y aspect ratio):
plot(c(-10,10), c(-10,10), type = "n", xlab = "x", ylab = "y", asp = 1)
## the x- and y-axis, and an integer grid
abline(h = 0, v = 0, col = "gray60")
text(1,0, "abline( h = 0 )", col = "gray60", adj = c(0, -.1))
abline(h = -10:10, v = -10:10, col = "lightgray", lty = 3)
# abline(a = 1, b = 2, col = 2)

## LINE 1
abline(a = 0, b = 0, col = 3, lwd = 5)
abline(a = 5, b = 0, col = 3, lwd = 3)
abline(a = -5, b = 0, col = 3, lwd = 3)
## LINE 2
abline(a = 0, b = 3, col = 4, lwd = 5)
abline(a = 5, b = 3, col = 4, lwd = 3)
abline(a = -5, b = 3, col = 4, lwd = 3)
## LINE 3
abline(a = 0, b = -3, col = 5, v =)
abline(a = 5, b = -3, col = 5)
abline(a = -5, b = -3, col = 5)
# text(1,3, "abline( 1, 2 )", col = 2, adj = c(-.1, -.1))

