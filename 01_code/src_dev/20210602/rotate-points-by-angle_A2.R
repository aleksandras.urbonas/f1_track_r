#sample data
set.seed(1) #for consistency of random-generated data
d <- matrix(c(sort(runif(50)),sort(runif(50))),ncol=2)

plot(d)

#rotation about point A
rotA <- function(d) {
  d.offset <- apply(d,2,function(z) z - z[1]) #offset data
  endpoint <- d.offset[nrow(d.offset),] #gets difference
  rot <- function(angle) matrix(
    c(cos(angle),-sin(angle),sin(angle),cos(angle)),nrow=2) #CCW rotation matrix
  if(endpoint[2] == 0) {
    return(d) #if y-diff is 0, then no action required
  } else if (endpoint[1] == 0) { 
    rad <- pi/2 #if x-diff is 0, then rotate by a right angle
  } else {rad <- atan(endpoint[2]/endpoint[1])}
  d.offset.rotate <- d.offset %*% rot(-rad) #rotation
  d.rotate <- sapply(1:2,function(z) d.offset.rotate[,z] + d[1,z]) #undo offset
  d.rotate
}

#results and plotting to check visually
d.rotate <- rotA(d)
plot(d.rotate)
abline(h=d[1,2])
