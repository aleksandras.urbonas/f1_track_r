# title: circle_points.R
# v: 2021-06-02T2022 AU

circle_points <- function(my_x, my_y, my_radius) {
  
  my_df_xy <- data.frame() # results
  
  for (this_degree in 1:360) {
    # this_degree <- 0 # test
    # this_degree <- 1 # test: OK
    # this_degree <- 30 # test TBC
    # this_degree <- 45 # test TBC
    # this_degree <- 60 # test cos: OK
    # this_degree <- 90 # test TBC
    # print(this_degree) # QC
    my_degree <- this_degree * pi/180
    # if(cos(my_degree)=='0.5') cat('60 degree angle')
    # my_radius*cos(my_degree) # 0.5
    # my_radius*sin(my_degree) # 0.8660254

	## calculate a coordinate for a point at degree `i`
    my_x_degree <- my_x + my_radius * cos(my_degree)
    my_y_degree <- my_y + my_radius * sin(my_degree)

    ## get current point
    my_xy <- cbind(x = my_x_degree, y = my_y_degree)

    ## store current point
    my_df_xy <- rbind(my_df_xy, my_xy)
  }
  
  ## return  
  return(my_df_xy)
}


## Test Call:
points(
  circle_points(my_x = 0, my_y = 0, my_radius = 10)
, col = 'purple'
, pch = '.'
)
