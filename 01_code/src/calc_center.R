# title  : calc_center.R
# version: 2022-08-12T1820 AU
# review : 2022-09-10T2205 AU


calc_center <- function(df, precision=4, SILENT=TRUE) {

  xc = round( ( abs(max(df$x)) - abs(min(df$x)) ) / 2, precision)
  yc = round( ( abs(max(df$y)) - abs(min(df$y)) ) / 2, precision)

  # xc = max(df$x) - ( abs(max(df$x)) - abs(min(df$x)) ) / 2
  # yc = max(df$y) - ( abs(max(df$y)) - abs(min(df$y)) ) / 2
  
  if (!SILENT) cat("track center: (", xc, ",", yc, ")", "\n")

  return(c(xc, yc))
}
