# title  : plot_track.R
# version: 2022-08-13T1159 AU
# review : 2022-09-11T1844 AU

# plot track wrapper
plot_track <- function(
    df
    , XLIM, YLIM
    , Cx, Cy
    , main
    , cut_last
) {
  plot_df(
    df=df
    , 1
    , xlim=XLIM
    , ylim=YLIM
    , main=main
    , cut_last=cut_last
    )
  
  # plot x and y axis
  abline(v=0, h=0, col="gray")
  
  # add center point
  points(Cx, Cy, col='green')
}
