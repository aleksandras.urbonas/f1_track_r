# title  : extend_rays.R
# version: 2022-09-10T1723 AU

SCALE = 1.08

#### Extend ray `CO`
# calc the extended vector
OC1xy = extend_ray(C_xy, O_xy, scale=SCALE*1.08) # long
OC2xy = extend_ray(C_xy, O_xy, scale=SCALE*0.88) # short
OC1x = OC1xy[1]; OC1y = OC1xy[2];
OC2x = OC2xy[1]; OC2y = OC2xy[2];

#### Extend ray `AO`
# calc the extended vector
OA1xy = extend_ray(O_xy, A_xy, scale=SCALE*1.08) # long
OA2xy = extend_ray(O_xy, A_xy, scale=SCALE*0.88) # short
OA1x = OA1xy[1]; OA1y = OA1xy[2];
OA2x = OA2xy[1]; OA2y = OA2xy[2];

#### Extend ray `OB`
# calc the extended vector
OB1xy = extend_ray(O_xy, B_xy, scale=SCALE*1.08) # long
OB2xy = extend_ray(O_xy, B_xy, scale=SCALE*0.88) # short
OB1x = OB1xy[1]; OB1y = OB1xy[2];
OB2x = OB2xy[1]; OB2y = OB2xy[2];
