# title: f1_track
# v: 2021-06-02 AU


# Goal
Create an F1 track for self-driving-car simulation


# Input
Given GPS points, create a track of width `W`


# Tasks
+ | read points: DONE
+ | points to lines: DONE
+ | parallel lines: ONGOING
+ | circle: DONE
* | detect angle between two lines: DONE
- | rotate track: TBC
- | draw a line in the middle of two lines: TBC
- | detect intersection of circle and parallel line
- | draw perpendicular line: TBC


# Sources:
1. Spanish GP: Coordinates: * 41*34`9.9``N, 2*15`26.9``E
URL: <https://en.wikipedia.org/wiki/Spanish_Grand_Prix#/media/File:Formula1_Circuit_Catalunya_2021.svg>
Data given in meters


2. find angle and distance between two points
URL: <https://stackoverflow.com/questions/48444003/creating-r-function-to-find-both-distance-and-angle-between-two-points>

# df=To-From